Name:           aboot-update
Version:        0.4
Release:        1%{?dist}
Summary:        Update aboot from kernel/initrd

License:        GPLv2+
Source1:        aboot-update
Source2:        90-aboot.install

Requires:       abootimg

%description

Aboot-update is a tool that given a kernel version creates an android
boot image (aboot) file called /boot/aboot-VERSION.img based on the
options specified in /boot/aboot.cfg.

There is also a kernel-install script (90-aboot.install) that automatically
runs this on each new kernel install if the kernel install layout is set
to aboot.

%prep
rm -rf %{name}-{%version}
mkdir %{name}-{%version}


%build
cd %{name}-{%version}

%install
cd %{name}-{%version}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_prefix}/lib/kernel/install.d
install -m755 %{SOURCE1} %{buildroot}%{_bindir}/aboot-update
install -m755 %{SOURCE2} %{buildroot}%{_prefix}/lib/kernel/install.d/90-aboot.install

%files
%{_bindir}/aboot-update
%{_prefix}/lib/kernel/install.d/90-aboot.install

%changelog
* Fri Jul 21 2023 Eric Curtin <ecurtin@redhat.com>
- we now use androidboot.slot_suffix= to check if we are aboot
  or not and ostree= to parse the stateroot.

* Sat Jul 15 2023 Eric Curtin <ecurtin@redhat.com>
- Add mkbootimg support and some initial boilerplate for
  vbmeta

* Tue Jul 04 2023 Eric Curtin <ecurtin@redhat.com>
- Substitute ostree= with ostree=aboot for this case, so
  ostree knows how to boot correctly.

* Mon Mar 20 2023 Eric Curtin <ecurtin@redhat.com>
- Changes to allow for relative paths and absolute paths when
  specifying dtbs

* Tue Mar 14 2023 Eric Curtin <ecurtin@redhat.com>
- try to create both links

* Mon Mar 13 2023 Eric Curtin <ecurtin@redhat.com>
- extra link to satisfy osbuild

* Wed Jan 11 2023 Eric Curtin <ecurtin@redhat.com>
- ostree enablement

* Fri Aug 19 2022 Alexander Larsson <alexl@redhat.com>
- Initial version
