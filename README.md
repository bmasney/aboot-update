# aboot-update

Aboot-update is a tool that given a kernel version creates an android
boot image (aboot) file called /boot/aboot-VERSION.img based on the
options specified in /boot/aboot.cfg.

There is also a kernel-install script (90-aboot.install) that automatically
runs this on each new kernel install if the kernel install layout is set
to aboot.
